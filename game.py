import os
import random
import pygame
from sprite import *

WIDTH = 1280
HEIGHT = 800

FPS = 60

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

game_folder = os.path.dirname(__file__)

pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Hakkero Wars")
clock = pygame.time.Clock()

all_sprites = pygame.sprite.Group()
veiri = Veiri()
tost = Tost()
all_sprites.add(veiri)
all_sprites.add(tost)

#Pattern 1
bullet_angle = 0


running = True
while running:
    clock.tick(FPS)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                pass
                
    #Update
    all_sprites.update()
    bullet_pattern.update()
    toastBullet.update()

    hits = pygame.sprite.spritecollide(tost, bullet_pattern, True, pygame.sprite.collide_circle)
    graze = pygame.sprite.spritecollide(tost, bullet_pattern, False)

    hitVeiri = pygame.sprite.spritecollide(veiri, toastBullet, True, pygame.sprite.collide_circle)

    for hits in hitVeiri:
        veiri.damage()

    #Draw/render
    screen.fill(BLACK)
    toastBullet.draw(screen)
    all_sprites.draw(screen)
    bullet_pattern.draw(screen)
    pygame.display.flip()

pygame.quit()
