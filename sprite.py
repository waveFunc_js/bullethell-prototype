import os
import pygame
import math

BLACK = (0, 0, 0)
RED = (255, 0, 0)

WIDTH = 1280
HEIGHT = 720

game_folder = os.path.dirname(__file__)

bullet_pattern = pygame.sprite.Group()
toastBullet = pygame.sprite.Group()

class Tost(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join(game_folder, "assets/tost/tostsmall.png")).convert()
        self.rect = self.image.get_rect()
        self.radius = 4
        pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.image.set_colorkey(BLACK)
        self.rect.center = (800 / 2, HEIGHT / 1.5)
        self.health = 1000
        self.speed = 5
        self.bulletTimeGap = 1

    def update(self):
        self.speedx = 0
        self.speedy = 0
        self.slowdown = 0
        self.bulletTimeGap = (self.bulletTimeGap + 1) % 5

        keystate = pygame.key.get_pressed()

        if keystate[pygame.K_z]:
            '''
            if(self.bulletTimeGap == 0):
                toastBullet.add(Bullet2(270, self.rect.centerx, self.rect.centery))
            '''
        if keystate[pygame.K_LSHIFT]:
            self.slowdown = 1
            self.image.blit( pygame.image.load(os.path.join(game_folder, "assets/tost/hitbox.png")).convert(), (13, 20))

        if keystate[pygame.K_LEFT]:
            self.speedx = -self.speed

            if self.slowdown == 1:
                self.speedx += 2

        if keystate[pygame.K_RIGHT]:
            self.speedx = self.speed

            if self.slowdown == 1:
                self.speedx -= 2

        if keystate[pygame.K_UP]:
            self.speedy = -self.speed

            if self.slowdown == 1:
                self.speedy += 2

        if keystate[pygame.K_DOWN]:
            self.speedy = self.speed

            if self.slowdown == 1:
                self.speedy -= 2

        self.rect.x += self.speedx
        self.rect.y += self.speedy

        if self.rect.right > 800:
            self.rect.right = 800
        
        if self.rect.left < 0:
            self.rect.left = 0

class Veiri(pygame.sprite.Sprite):
    
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join(game_folder, "assets/veiri/veirismall.png")).convert()
        self.radius = 16
        self.rect = self.image.get_rect()
        pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.image.set_colorkey(BLACK)
        self.rect.center = (800 / 2, HEIGHT / 4)
        self.health = 1000
        self.counter = 1
        self.timing = 0

    def update(self):

        if self.counter == 1:
            self.rect.centerx == 1
            if self.timing % 10 == 0:
                self.pattern1()

        if self.counter == 2:
            if self.timing % 10 == 0:
                self.pattern1()
            if self.timing % 50 == 0:
                self.pattern2()

        self.timing += 1

        if self.health < 800:
            self.counter = 2

        if self.health < 0:
            self.kill()

    #spiral
    def pattern1(self):
        bullet_angle = self.timing
        for x in range(12):
            bullet_pattern.add(Bullet1(bullet_angle, self.rect.centerx, self.rect.centery))
            bullet_angle = (bullet_angle + 30)

    def damage(self):
        self.health -= 5


#normal bullet
class Bullet1(pygame.sprite.Sprite):
    
    def __init__(self, angle, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join(game_folder, "assets/weapon/bullet1.png")).convert()
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.radius = 3
        pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.image.set_colorkey(BLACK)
        self.angle = angle
        self.speedx = 10 * math.cos(math.radians(self.angle)) 
        self.speedy = 10 * math.sin(math.radians(self.angle))
        self.posx = self.rect.centerx
        self.posy = self.rect.centery
        self.part1 = 0

    def update(self):

        if self.part1 < 300:
            self.posx += self.speedx * math.sin(math.radians(self.part1)) 
            self.posy += self.speedy * math.cos(math.radians(self.part1)) 
            self.part1 += 3

        elif self.part1 >= 300 and self.part1 < 500:
            self.posx -= self.speedx * math.sin(math.radians(self.part1)) 
            self.posy += self.speedy * math.cos(math.radians(self.part1)) 
            self.part1 += 5

        else:   
            self.posx += self.speedx
            self.posy += self.speedy

        self.rect.center = (self.posx, self.posy)
        if(self.rect.right > 800 or self.rect.left < 0 or self.rect.top > 720 or self.rect.top < -100):
            self.kill()

